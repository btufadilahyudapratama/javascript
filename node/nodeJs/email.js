var nodemailer = require('nodemailer');

var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'btufadilahyudapratama@gmail.com',
        pass: 'P455w0rd'
    }
});

var mailOptions = {
    from: 'yudapratamawork@gmail.com',
    to: 'fadilahyudapratamafyp@gmail.com',
    subject: 'Sending Email using Node.js',
    text: 'That was easy!'
};

transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
        console.log(error);
    } else {
        console.log('Email sent: ' + info.response);
    }
});