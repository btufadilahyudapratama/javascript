var events = require('events');
var eventEmitter = new events.EventEmitter();

//Create an event handler:
var ayu = function () {
    console.log('Ayuni Afifah dan Fadilah Yuda Pratama');
}

//Assign the event handler to an event:
eventEmitter.on('yudha', ayu);

//Fire the 'scream' event:
eventEmitter.emit('yudha');